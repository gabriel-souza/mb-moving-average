# mb-moving-averages 📈
![version](https://img.shields.io/gitlab/v/tag/gabriel-souza/mb-moving-average)

## Definição

Consiste em uma estrutura que é responsavel por calcular para cada dia no ultimo ano a Média Móvel Simples (MMS) de 20, 
50 e 200 dias. Persistir em um banco de dados e disponibilizar as médias para consulta em uma API.

## Média Móvel Simples (MMS)
O calculo das médias móveis se trata de pegar um trecho de tempo anterior ao dia selecionado e
fazer uma media aritmética. Por exemplo, para calcular a MMS de 5 dias do valor em Reais do Bitcoin para o dia
15/05, deve-se:
* Pegar o valor do Bitcoin em reais para os dias 15/05, 14/05, 13/05, 12/05 e 11/05;
* Somar todos os valores;
* Dividir por 5 (a quantiade de dias).

## Arquitetura

Para a solução desse problema, decidiu-se dividir as responsabilidades em dois microsserviços: 

- O `mb-moving-averages-api`, ficou responsável pela disponibilização da API de MMS de inserção e listagem e pela 
consistência desses dados no banco de dados;

- O `mb-moving-averages-data-load`, ficou responsável pelo cálculo dos MMS e o envio desses dados para a API de inserção. 
Esse serviço pode ser parametrizado para fazer uma carga cheia dos 365 dias na base ou apenas o calculo do dia anterior, 
para que assim, possam ser criados dois workloads, um como um cronjob contemplando as cargas diárias e outro apenas para 
ser rodado uma vez, com a carga inicial.

Para um detalhamento maior do funcionamento dos serviços pode-se olhar as docs dos respectivos projetos:
* [mb-moving-averages-api](api/README.md)
* [mb-moving-averages-data-load](data_load/README.md)


![diagrama](resources/arquitetura-mms.png)

# Buildando e rodando a estrutura

O projeto já está de uma forma plug and play utilizando docker. Então, para rodar o projeto inteiro basta executar o 
comando `docker compose up -d`. 

O compose desse projeto já está configurado para subir uma instância de postgres, e os 
dois serviços utilizando as variaveis de ambientes configuradas no arquivo .env de cada projeto

A senha do postgres padrão está como `postgres`, mas pode ser mudada enviando a variavel `POSTGRES_PASSWORD` para o 
compose e em seguida mudando na variavel de ambiente do projeto api ([`api/.env`](api/.env)). 

Para mais detalhes de como rodar a estrutura localmente ou de forma separada consulte a documentação de cada projeto. 