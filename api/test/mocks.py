from datetime import date, timedelta
from uuid import uuid4

from factory import Factory, Sequence
from factory.fuzzy import FuzzyDecimal

from src.schemas import Pair
from src.schemas.moving_average import MovingAverageCreateRequest, MovingAverage


class MovingAverageCreateRequestFactory(Factory):
    class Meta:
        model = MovingAverageCreateRequest

    timestamp = Sequence(lambda i: date.today() - timedelta(days=i))
    mms_20 = FuzzyDecimal(1_000_000_000.00)
    mms_50 = FuzzyDecimal(1_000_000_000.00)
    mms_200 = FuzzyDecimal(1_000_000_000.00)


class MockExec:
    @staticmethod
    def all():
        return [
            MovingAverage(
                id=uuid4(),
                pair=Pair.BRLBTC,
                timestamp=date.today() - timedelta(days=1),
                mms_20=50.5555,
                mms_50=46.111,
                mms_200=44.99
            ),
            MovingAverage(
                id=uuid4(),
                pair=Pair.BRLBTC,
                timestamp=date.today() - timedelta(days=2),
                mms_20=50.4444,
                mms_50=45.111,
                mms_200=43.99
            ),
            MovingAverage(
                id=uuid4(),
                pair=Pair.BRLBTC,
                timestamp=date.today() - timedelta(days=3),
                mms_20=51.5555,
                mms_50=47.111,
                mms_200=45.99
            ),
            MovingAverage(
                id=uuid4(),
                pair=Pair.BRLBTC,
                timestamp=date.today() - timedelta(days=4),
                mms_20=55.5555,
                mms_50=43.111,
                mms_200=4.99
            )
        ]
