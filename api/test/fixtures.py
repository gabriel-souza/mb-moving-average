from unittest.mock import MagicMock

import pytest
from sqlmodel.ext.asyncio.session import AsyncSession
from starlette.testclient import TestClient

from src.app import app
from src.database.config import get_session
from test.mocks import MockExec


@pytest.fixture(name="session")
async def session_fixture() -> AsyncSession:

    session = MagicMock(AsyncSession)

    session.exec.return_value = MockExec()

    yield session


@pytest.fixture(name="client")
async def client_fixture(session: AsyncSession):
    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)

    yield client

    app.dependency_overrides.clear()
