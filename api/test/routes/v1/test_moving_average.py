import json
from datetime import datetime, timedelta

from fastapi import status

from src.settings import settings
from src.utils import timestamp_for_current_day_at_hour_zero
from test.fixtures import *
from test.mocks import MovingAverageCreateRequestFactory


def test_when_range_20_data_is_correctly_informed_should_return_success(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': (datetime.today() - timedelta(days=4)).timestamp(),
            'range': 20
        }
    )

    response_body = response.json()

    # assert
    assert response.status_code == status.HTTP_200_OK

    assert response_body[0]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=1)
    assert response_body[0]['mms'] == 50.5555

    assert response_body[1]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=2)
    assert response_body[1]['mms'] == 50.4444

    assert response_body[2]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=3)
    assert response_body[2]['mms'] == 51.5555

    assert response_body[3]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=4)
    assert response_body[3]['mms'] == 55.5555


def test_when_range_50_data_is_correctly_informed_should_return_success(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': (datetime.today() - timedelta(days=4)).timestamp(),
            'range': 50
        }
    )

    response_body = response.json()

    # assert
    assert response.status_code == status.HTTP_200_OK

    assert response_body[0]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=1)
    assert response_body[0]['mms'] == 46.111

    assert response_body[1]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=2)
    assert response_body[1]['mms'] == 45.111

    assert response_body[2]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=3)
    assert response_body[2]['mms'] == 47.111

    assert response_body[3]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=4)
    assert response_body[3]['mms'] == 43.111


def test_when_range_200_data_is_correctly_informed_should_return_success(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': (datetime.today() - timedelta(days=4)).timestamp(),
            'range': 200
        }
    )

    response_body = response.json()

    # assert
    assert response.status_code == status.HTTP_200_OK

    assert response_body[0]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=1)
    assert response_body[0]['mms'] == 44.99

    assert response_body[1]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=2)
    assert response_body[1]['mms'] == 43.99

    assert response_body[2]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=3)
    assert response_body[2]['mms'] == 45.99

    assert response_body[3]['timestamp'] == timestamp_for_current_day_at_hour_zero(minus_days=4)
    assert response_body[3]['mms'] == 4.99


def test_when_from_is_greater_then_to_should_return_error(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': datetime.today().timestamp(),
            'to': (datetime.today() - timedelta(days=4)).timestamp(),
            'range': 200
        }
    )

    # assert
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_when_no_range_is_informed_should_return_error(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': (datetime.today() - timedelta(days=4)).timestamp(),
            'to': datetime.today().timestamp(),
        }
    )

    response_body = response.json()

    # assert
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_when_no_from_is_informed_should_return_error(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'to': datetime.today().timestamp(),
            'range': 200
        }
    )

    response_body = response.json()

    # assert
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_when_from_is_greater_then_365_days_should_return_error(client: TestClient):
    # act
    response = client.get(
        '/v1/BRLBTC/mms',
        params={
            'from': (datetime.today() - timedelta(days=366)).timestamp(),
            'to': datetime.today().timestamp(),
            'range': 50
        }
    )

    # assert
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


def test_when_insertion_bulk_is_valid_and_under_365_of_length_should_return_sucess(client):
    # act
    response = client.post(
        '/v1/BRLBTC/mms/batch',
        data=json.dumps([MovingAverageCreateRequestFactory().dict() for _ in range(365)], default=str)
    )

    assert response.status_code == status.HTTP_200_OK
    assert len(response.json()) == 365


def test_when_insertion_bulk_is_valid_and_over_365_of_length_should_return_error(client):
    # act
    response = client.post(
        '/v1/BRLBTC/mms/batch',
        data=json.dumps(
            [MovingAverageCreateRequestFactory().dict() for _ in range(settings.max_bulk_insertions + 1)],
            default=str
        )
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
