# Moving Averages API 📈
![coverage](test/resources/coverage.svg)

## Definição

Esse projeto é responsavel pelo dominio da base de dados que guarda os dados das médias móveis 
na conversão entre moedas Crypto e Fiat. Nesse primeiro momento tratamos apenas com dois pares 
de moedas, sendo eles: Ethereum e Bitcoin para Real Brasileiro (`BRLETH` e `BRLBTC`). Vale destacar que essa 
estrutura de serviço também permite o cadastro de novos pares de uma forma facilitada.

## Arquitetura

Para essa POC foram utilizadas tecnologias que estão em fase de amadurecimento, por isso ainda não são recomendadas para ambientes produtivos. Mas, por serem extremamente promissoras e performáticas foi decidido utilizá-las.

### Dentre elas algumas são:
>### Desenvolvimento 
> 
> 
>* [FastApi](https://fastapi.tiangolo.com/): Framework de desenvolvimento de APIs que se utiliza da bilioteca 
AsyncIO do Python para tirar maximo proveito da linguagem, tendo uma performance em par com grandes linguagens 
como, por exemplo, Golang e Node.
>
>
>* [SQLModel](https://sqlmodel.tiangolo.com/): ORM baseada no [SQLAlchemy](https://www.sqlalchemy.org/) que junto com 
[Pydantic](https://pydantic-docs.helpmanual.io/usage/models/) entrega uma integração transparente entre os modelos da 
API e os modelos do banco de dados com o FastApi.
> 
> 
> * [AsyncPG](https://github.com/MagicStack/asyncpg): Para conseguir trabalhar de uma forma assíncrona e transformar as 
operações no PostgreSQL não blocantes, essa biblioteca foi utilizada em conjunto com o [AsyncIO do SQLAlchemy](https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html). 

A motivação de utilizar essas bibliotecas em especifico foi para usar ao maximo a assíncronidade do Python deixando assim um programa mais resiliente.

> ### Testes
> 
> 
> * [pytest](https://docs.pytest.org/en/6.2.x/): Com sua facilidade para declarar e utilizar fixtures, o desenvolvimento de testes para FastAPI ficou muito mais simples.
>
>
> * [factory-boy](https://factoryboy.readthedocs.io/en/stable/): Para um mock mais simplificado e randomico foi utilizado o factory-boy que prove 
Factories de objetos mockando dados aleatóriamente utilizando o [Faker](https://faker.readthedocs.io/en/master/).

Desenvolver testes unitários para FastAPI se torna muito simples, pois ele provê uma biblioteca extremamente robusta de testes. Mas, para mocking 
não há uma forma simplificada de uso, então trabalhou-se com o factory boy para criação de objetos de testes.

## Rodar os testes

Para rodar os testes unitários do projeto basta executar o comando `make run-tests` ou `python3 -m pytest test`

## Build do Projeto

### Pré requisitos:

* Ter um arquivo `.env` na raiz do projeto informando as seguintes variáveis:

|      Variável     |Obrigatória| Default |
|-------------------|:---------:|:-------:|
|APP_HOST           |    Não    |`0.0.0.0`|
|APP_PORT           |    Não    | `8000`  |
|BASE_PATH          |    Não    |   ` `   |
|WORKERS            |    Não    |   `1`   |
|AUTO_RELOAD        |    Não    | `False` |
|DEBUG              |    Não    | `False` |
|MAX_BULK_INSERTIONS|    Não    |  `365`  |
|DB_HOST            |    Sim    |    -    |
|DB_USER            |    Sim    |    -    |
|DB_PASSWORD        |    Sim    |    -    |
|DB_NAME            |    Sim    |    -    |

* Ter [Make](https://www.gnu.org/software/make/), [Python 3.9>](https://www.python.org/downloads/) e [pip](https://pip.pypa.io/en/stable/installation/) caso queira buildar diretamente no computador nativo;
* Ou ter [Docker](https://docs.docker.com/get-docker/) instalado caso queira buildar containerizado.

### Build Containerizado

Para rodar ambos os projetos e o banco necessário para as aplicações, navegue até a raiz do repositório (`mb-moving-averages/`) e execute o comando `docker compose up -d`.  Caso queira rodar apenas o projeto de API é possível adicionar os parâmetros `api db` no comando, exemplo `docker compose -d api db`.

### Diretamente pelo Python

Na raiz do projeto (`mb-moving-averages/api`) existe uma Makefile com funções para auxiliar o build do projeto.
Basta rodar `make run-dev`, que builda, exporta as variàveis necessárias e serve a aplicação na porta 8000 
(caso a variável `APP_HOST` não seja alterada).

Para testar o projeto rodando, é possível entrar na url [/redoc](http://localhost:8000/redoc) para ter 
acesso ao swagger do projeto com a documentação das APIs disponíveis.