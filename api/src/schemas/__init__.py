from enum import Enum


class Pair(str, Enum):
    BRLBTC = 'BRLBTC'
    BRLETH = 'BRLETH'


class Range(str, Enum):
    TWENTY = "20"
    FIFTY = "50"
    TWO_HUNDRED = "200"
