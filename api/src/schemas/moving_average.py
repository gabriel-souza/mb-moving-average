import uuid
from datetime import datetime, date, timedelta
from decimal import Decimal
from typing import Optional, Type

from fastapi import Query
from pydantic import validator
from sqlalchemy import UniqueConstraint
from sqlmodel import SQLModel, Field

from src.schemas import Pair, Range
from src.utils import get_yesterday


class MovingAverageQueryString(SQLModel):
    amount_of_days: Range = Query(..., alias='range')
    start_date: date = Query(..., alias='from')
    end_date: date = Query(None, alias='to')

    @validator('start_date')
    def validate_start_date(cls: Type["SQLModel"], value: date):
        if value < (date.today() - timedelta(days=365)):
            raise ValueError('from has to be within 365 day from today')

        return value

    @validator('end_date')
    def validate_end_date(cls: Type["SQLModel"], v: date, values: dict):
        if not v:
            v = get_yesterday()
        if values.get('start_date') and v <= values['start_date']:
            raise ValueError('from has to be smaller than to')
        return v


class MovingAverageBase(SQLModel):
    timestamp: date
    mms_20: Decimal
    mms_50: Decimal
    mms_200: Decimal


class MovingAverage(MovingAverageBase, table=True):
    __table_args__ = (UniqueConstraint('pair', 'timestamp'),)
    id: Optional[uuid.UUID] = Field(default=None, primary_key=True)
    pair: Pair


class MovingAverageGetResponse(SQLModel):
    timestamp: datetime
    mms: Decimal

    class Config:
        json_encoders = {
            datetime: lambda v: int(v.timestamp())
        }


class MovingAverageCreateRequest(MovingAverageBase):
    pass


class MovingAverageCreateResponse(MovingAverageBase):
    class Config:
        json_encoders = {
            datetime: lambda v: int(v.timestamp())
        }
