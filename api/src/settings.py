from pydantic import BaseSettings


class Settings(BaseSettings):
    app_host: str = '0.0.0.0'
    app_port: int = 8000
    base_path: str = ''

    workers: int = 1
    auto_reload: bool = False
    debug: bool = False

    db_host: str
    db_user: str
    db_password: str
    db_name: str

    max_bulk_insertions: int = 365

    def get_db_uri(self):
        return f'postgresql+asyncpg://{self.db_user}:{self.db_password}@{self.db_host}/{self.db_name}'


settings = Settings()

