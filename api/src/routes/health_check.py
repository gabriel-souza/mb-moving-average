from fastapi import APIRouter, status

from src import __version__

router = APIRouter()


@router.get(
    '/health-check',
    summary='Verifica se a aplicação está rodando.',
    status_code=status.HTTP_200_OK
)
def get():
    return {'message': 'OK', 'version': __version__}

