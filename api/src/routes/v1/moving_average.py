from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from src.database.config import get_session
from src.schemas import Pair
from src.schemas.moving_average import MovingAverageCreateRequest, MovingAverageCreateResponse, \
    MovingAverageQueryString, MovingAverageGetResponse
from src.services.moving_average import MovingAverageService
from src.settings import settings

router = APIRouter()


@router.get('/', response_model=List[MovingAverageGetResponse], status_code=status.HTTP_200_OK)
async def get(pair: Pair,
              query: MovingAverageQueryString = Depends(MovingAverageQueryString),
              session: AsyncSession = Depends(get_session)
              ):

    return await MovingAverageService.get(pair, query, session)


@router.post('/batch', response_model=List[MovingAverageCreateResponse], status_code=status.HTTP_200_OK)
async def post(pair: Pair, payload: List[MovingAverageCreateRequest], session: AsyncSession = Depends(get_session)):
    if len(payload) > settings.max_bulk_insertions:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail='Too many insertions')

    return await MovingAverageService.insert_bulk(pair, payload, session)

