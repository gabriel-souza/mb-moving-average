from fastapi import APIRouter

from src.routes.v1 import moving_average

router = APIRouter()

router.include_router(moving_average.router, prefix='/{pair}/mms')
