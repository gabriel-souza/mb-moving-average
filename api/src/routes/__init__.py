from fastapi import APIRouter

from src.routes import health_check
from src.routes.v1 import router as v1_router

api_router = APIRouter()

api_router.include_router(v1_router, prefix='/v1')
api_router.include_router(health_check.router)
