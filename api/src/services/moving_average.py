from datetime import datetime, timezone
from typing import List
from uuid import uuid4

from sqlalchemy import between
from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession
from sqlmodel.sql.expression import Select

from src.schemas import Pair, Range
from src.schemas.moving_average import MovingAverage, MovingAverageQueryString, MovingAverageGetResponse


class MovingAverageService:

    @staticmethod
    async def insert_bulk(pair, payload, session) -> List[MovingAverage]:
        response = []

        for data in payload:
            item = MovingAverage(id=uuid4(), pair=pair, **data.dict())
            session.add(item)
            response.append(item)

        await session.commit()
        for item in response:
            await session.refresh(item)

        return response

    @staticmethod
    async def get(pair: Pair, query: MovingAverageQueryString, session: AsyncSession) -> List[MovingAverageGetResponse]:
        statement = select(MovingAverage) \
                    .where(between(MovingAverage.timestamp, query.start_date, query.end_date)) \
                    .where(MovingAverage.pair == pair)

        result = await session.exec(statement)
        result = result.all()

        moving_averages = []

        for mms in result:
            ranges = {
                Range.TWENTY: mms.mms_20,
                Range.FIFTY: mms.mms_50,
                Range.TWO_HUNDRED: mms.mms_200
            }

            moving_averages.append(
                MovingAverageGetResponse(
                    timestamp=datetime.combine(mms.timestamp, datetime.min.time(), tzinfo=timezone.utc),
                    mms=ranges[query.amount_of_days]
                )
            )

        return moving_averages
