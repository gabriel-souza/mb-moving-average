import time

import uvicorn
from fastapi import FastAPI, Request
from pydantic import ValidationError
from sqlalchemy.exc import IntegrityError
from starlette.responses import JSONResponse

from src import __version__
from src.database.config import init_db
from src.routes import api_router
from src.settings import settings

app = FastAPI(
    title='Moving Averages',
    version=__version__,
    description='Gets the moving average for the selected days',
    docs_url=f'{settings.base_path}/docs',
    redoc_url=f'{settings.base_path}/redoc',
    openapi_url=f'{settings.base_path}/openjson',
    debug=settings.debug,
)

app.include_router(api_router)


@app.on_event("startup")
async def on_startup():
    await init_db()


@app.exception_handler(IntegrityError)
def handle_db_error(request: Request, exc: IntegrityError):
    return JSONResponse(
        status_code=400,
        content={"message": "data could not be inserted."}
    )


@app.exception_handler(ValidationError)
def handle_validation_error(request: Request, exc: ValidationError):
    return JSONResponse(
        status_code=422,
        content=exc.json()
    )


if __name__ == '__main__':
    uvicorn.run(
        'src.app:app',
        host=settings.app_host,
        port=settings.app_port,
        debug=settings.debug,
        log_level='info',
        access_log=True,
        workers=settings.workers,
        timeout_keep_alive=30,
        reload=settings.auto_reload
    )
