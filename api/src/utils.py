from datetime import date, timedelta, datetime, timezone


def get_yesterday():
    return date.today() - timedelta(days=1)


def timestamp_for_current_day_at_hour_zero(minus_days: int = 0):
    return datetime.combine(
        date.today() - timedelta(days=minus_days),
        datetime.min.time(),
        tzinfo=timezone.utc
    ).timestamp()
