# Moving Average Data Load 📥
![coverage](test/resources/coverage.svg)

## Definição

Esse projeto é responsavel por fazer o cálculo e a carga inicial e incremental das médias móveis que serão consumidas. 
Ele consiste em um worker Python que será agendado para rodar todos os dias, parametrizado para carga incremental e que 
pode ser rodado como uma carga completa dos últimos 365 dias, se for necessário.


## Arquitetura

### Funcionalidades
Esse serviço lida com uma grande quantidade de dados, que realiza operações em massa e por isso foi pensado em utilizar 
pandas para a manipulação deles. Todo o cálculo é feito de uma forma genérica com a faixa de datas parametrizadas. 
E é retornado o cálculo de cada uma das médias móveis para 20, 50 e 200 dias.


### Dependências Externas
Esse microsserviço possuí duas dependências externas, uma na API de candles, que pode ficar indisponível a qualquer 
momento, e outra na API mms (outra API nesse repositório). Essas dependências são completamente blockantes para o 
processo como um todo. Logo, como estratégia de manutenção, temos retentativas na chamada. 

Caso na chamada da candles seja identificado a ausência de algum dia, implementaremos uma estratégia de comunicação por
Slack informando essa quantidade de dias faltantes no processamento.

### HTTPx
Além do pandas, para as chamadas HTTP, que é o processo de IO mais intenso na execução, foi utilizada a biblioteca HTTPx 
que conta com uma interface muito parecida com o requests (biblioteca muito usada para requests HTTP em Python), porém 
utiliza chamadas assíncronas aproveitando a biblioteca asyncIO, tornando requests múltiplas em concorrentes e 
acelerando o processamento.


### Conexão com o Banco
Considerando que o único processo que terá alta demanda para a outra API será a carga inicial, enquanto as incrementais 
terão baixa demanda, foi optado por abstrair a conexão com o banco de dados toda na API mms para que um microsserviço 
não invada o contexto do outro.

### Melhorias Futuras
Hoje não é mantido os dias que não foram processados para que sejam reprocessados no próximo batch. Foi idealizado desse 
controle ser feito utilizando um redis para gerenciar a entrada dos dias, e caso algum dia não seja inserido, uma flag 
para esse dia será subida, para que no próximo processamento, todas as flags sejam reprocessadas e caso haja sucesso,
essa flag será removida.  

O levantamento de dias que não foram retornados pela API candle também não foi implementado, porém a forma de 
implementar isso seria basicamente, fazer um diff entre um date_range dos dias contemplados e o dataframe dos dias 
calculados. Caso algum index estivesse faltando, deve-se levantar uma notificação ou um evento.

## Rodar os testes

Para rodar os testes unitários do projeto basta executar o comando `make run-tests` ou `python3 -m pytest test`

## Build do Projeto

### Pré requisitos:

* Ter um arquivo `.env` na raiz do projeto informando as seguintes variáveis:

|Variável |Obrigatória|              Default               |
|---------|:---------:|:----------------------------------:|
|MB_URL   |    Não    |https://mobile.mercadobitcoin.com.br|
|MMS_URL  |    Não    |          http://api:8000           |
|OPERATION|    Não    |              full_load             |

* Ter [Make](https://www.gnu.org/software/make/), [Python 3.9>](https://www.python.org/downloads/) e [pip](https://pip.pypa.io/en/stable/installation/) caso queira buildar diretamente no computador nativo;
* Ou ter [Docker](https://docs.docker.com/get-docker/) instalado caso queira buildar containerizado.

### Build Containerizado

Para rodar ambos os projetos e o banco necessário para as aplicações, navegue até a raiz do repositório 
 (`mb-moving-averages/`) e execute o comando `docker compose up -d`.

### Diretamente pelo Python

Na raiz do projeto (`mb-moving-averages/data_load`) existe uma Makefile com funções para auxiliar o build do projeto.
Basta rodar `make run-dev`, que builda, exporta as variàveis necessárias e começa o processamento.