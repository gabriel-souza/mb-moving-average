from datetime import date, timedelta, datetime, timezone
from typing import List

from factory import Factory, Sequence
from factory.fuzzy import FuzzyDecimal
from pydantic import BaseModel

from src.schemas import Candle


class CandleResponse(BaseModel):
    status_code: int = 100
    candles: List[Candle]


class CandleFactory(Factory):
    class Meta:
        model = Candle

    timestamp = Sequence(
        lambda i: datetime.combine(date.today(), datetime.min.time(), tzinfo=timezone.utc) - timedelta(days=i + 1)
    )
    close = FuzzyDecimal(1_000_000_000.00)
