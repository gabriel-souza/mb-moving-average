import json
import re

import pytest
from pytest_httpx import HTTPXMock

from src.app import main
from src.schemas import Pair
from src.settings import settings, Operation
from test.mocks import CandleResponse, CandleFactory


@pytest.mark.asyncio
async def test_when_load_is_full_should_parse_all_365_days(httpx_mock: HTTPXMock):
    # arrange
    settings.operation = Operation.FULL_LOAD
    btc_candles_mock = CandleResponse(
        candles=CandleFactory.create_batch(565)
    )
    CandleFactory.reset_sequence()

    eth_candles_mock = CandleResponse(
        candles=CandleFactory.create_batch(565)
    )
    CandleFactory.reset_sequence()

    httpx_mock.add_response(
        url=re.compile(f'{settings.candles_url}/v4/{Pair.BRLBTC.value}/candle*'),
        data=btc_candles_mock.json()
    )

    httpx_mock.add_response(
        url=re.compile(f'{settings.candles_url}/v4/{Pair.BRLETH.value}/candle*'),
        data=eth_candles_mock.json()
    )

    httpx_mock.add_response(url=f'{settings.mms_url}/v1/{Pair.BRLBTC.value}/mms/batch')
    httpx_mock.add_response(url=f'{settings.mms_url}/v1/{Pair.BRLETH.value}/mms/batch')

    # act
    await main()

    requests_btc = httpx_mock.get_requests(url=f'{settings.mms_url}/v1/{Pair.BRLBTC.value}/mms/batch')
    requests_eth = httpx_mock.get_requests(url=f'{settings.mms_url}/v1/{Pair.BRLETH.value}/mms/batch')
    body_btc = json.load(requests_btc[0])
    body_eth = json.load(requests_eth[0])

    # assert
    assert len(requests_btc) == 1
    assert len(requests_eth) == 1

    assert len(body_btc) == 365
    assert len(body_eth) == 365
    # cleanup
    CandleFactory.reset_sequence(1)
    httpx_mock.reset(True)
