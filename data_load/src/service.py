from src.parsers import calc_mms
from src.settings import settings
from src.talkers.candles import get_candles
from src.talkers.moving_average import save_moving_average


class MovingAverageService:

    async def initial_load(self):
        pair_candles = await get_candles(
            int(settings.full_load_range.timestamp()),
            int(settings.yesterday.timestamp())
        )
        await self.__save(pair_candles)

    async def incremental_load(self):
        pair_candles = await get_candles(
            int(settings.partial_range.timestamp()),
            int(settings.yesterday.timestamp())
        )
        await self.__save(pair_candles)

    async def __save(self, pair_candles):
        pair_mms = calc_mms(pair_candles)
        await save_moving_average(pair_mms)
