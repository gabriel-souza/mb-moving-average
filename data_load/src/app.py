import asyncio
import logging

from src.service import MovingAverageService
from src.settings import Operation, settings


async def main():
    logging.basicConfig(level=logging.INFO, format="[%(asctime)s]: %(levelname)s - %(message)s")

    service = MovingAverageService()

    operation = {
        Operation.FULL_LOAD: service.initial_load,
        Operation.PARTIAL_LOAD: service.incremental_load
    }

    await operation[settings.operation]()

if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
