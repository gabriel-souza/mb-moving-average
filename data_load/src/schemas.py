from datetime import datetime
from decimal import Decimal
from enum import Enum
from typing import List

from pydantic import BaseModel


class Pair(Enum):
    BRLBTC = 'BRLBTC'
    BRLETH = 'BRLETH'


class Candle(BaseModel):
    timestamp: datetime
    close: Decimal


class BaseCandle(BaseModel):
    candles: List[Candle]
