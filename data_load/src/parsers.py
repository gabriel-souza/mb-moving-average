import logging

import pandas as pd

from src.schemas import BaseCandle, Pair


def calc_mms(base_candles: dict[Pair, BaseCandle]) -> dict[Pair, str]:

    moving_averages = {}

    for pair, candles in base_candles.items():
        df = pd.DataFrame(candles.dict()['candles'])
        df.set_index('timestamp')

        logging.info(f'Calculating mms for {pair.value}')

        df['pair'] = pair.value
        df['mms_20'] = df.close.rolling(20).mean()
        df['mms_50'] = df.close.rolling(50).mean()
        df['mms_200'] = df.close.rolling(200).mean()

        df.drop('close', inplace=True, axis=1)
        df.dropna(inplace=True)
        moving_averages[pair] = df.iloc[:-1].to_json(orient='records')

        logging.debug(f'payload for insertion: {moving_averages[pair]}')
        logging.info(f'Ended calculations for {pair.value}')

    return moving_averages
