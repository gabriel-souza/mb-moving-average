import logging

import httpx

from src.schemas import BaseCandle, Pair
from src.settings import settings


async def get_candles(start_date, end_date) -> dict[Pair, BaseCandle]:
    transport = httpx.AsyncHTTPTransport(retries=5)

    async with httpx.AsyncClient(transport=transport) as client:
        candles = {}
        for pair in Pair:
            response = await client.get(
                f'{settings.candles_url}/v4/{pair.value}/candle',
                params={
                    'from': start_date,
                    'to': end_date,
                    'precision': '1d'
                }
            )

            if response.status_code >= 400:
                logging.error('Candles is Unavaliable')

            response = response.json()

            if response['status_code'] != 100:
                logging.error('Invalid data inputed on candles')

            candles[pair] = BaseCandle.parse_obj(response)

        return candles
