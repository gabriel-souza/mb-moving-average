import logging

import httpx

from src.schemas import Pair
from src.settings import settings


async def save_moving_average(mms: dict[Pair, str]):
    transport = httpx.AsyncHTTPTransport(retries=5)
    async with httpx.AsyncClient(transport=transport) as client:
        for pair, moving_averages in mms.items():
            response = await client.post(
                f'{settings.mms_url}/v1/{pair.value}/mms/batch',
                content=moving_averages
            )

            if response.status_code >= 400:
                logging.error(f"Couldn't insert mms for {pair}.")
                continue

            logging.info(f'Success for {pair}.')
