from datetime import datetime, timedelta
from enum import Enum

from pydantic import BaseSettings


class Operation(Enum):
    FULL_LOAD = 'full_load'
    PARTIAL_LOAD = 'partial_load'


class EnvSettings(BaseSettings):
    candles_url: str = 'https://mobile.mercadobitcoin.com.br'
    mms_url: str = 'http://api:8000'
    operation: Operation = Operation.FULL_LOAD


class Settings(EnvSettings):
    full_load_range: datetime = datetime.today() - timedelta(days=557)
    yesterday: datetime = datetime.today().replace(hour=23, minute=59, second=59) - timedelta(days=1)
    a_year_ago: datetime = datetime.today() - timedelta(366)
    partial_range: datetime = datetime.today() - timedelta(202)


settings = Settings()
